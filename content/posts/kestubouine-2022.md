---
title: "Participation au festival Kestu bouine le 30 avril 2022 !"
tags: ["Evenement", "Festival"]
date: "2022-04-30"
---

![affiche](https://gitlab.com/fluidlog/sitewebml72/-/raw/main/content/posts/img/kestubouine.png)

# Participation du collectif ML72
Nous sommes invités à participer au festival **Kestu bouine** à **chahaignes (72052)** le 30 avril 2022.
Un millier de personnes attendues !
L'objectif est de répondre aux questions des festivaliers au sujet des monnaies alternatives et plus précisément de la monnaie libre.
Nous aurons un emplacement dans le village associatif toute la journée et nous pourrons rester le soir pour les concerts.

# Communication externe
* Pour ceux qui sont sur Facebook
  * La page : https://www.facebook.com/kestubouine
  * L'événement : https://www.facebook.com/events/1040642906663049
* [Article dans Actu.fr](https://actu.fr/pays-de-la-loire/chahaignes_72052/sarthe-ils-sont-de-retour-pour-une-edition-speciale-du-festival-kestu-bouine_48861189.html)

# Où
Lieu-dit Asnière, 72340 Chahaignes, France

# Programme du festival
[recopié de la page Facebook le 23/04/2022]

## Kestu Bouine ? cuvée spéciale 12 ans !
* SAMEDI 30 AVRIL 2022
* CHAHAIGNES (72) - Gite d'Asnière
* Prix libre de 15h à 20h / 10€ après 20h
* Fête populaire agri-culturelle à circuits-court.
Concerts / Marché de producteurs et d'artisans locaux / Villages associatif / Animations et spectacle jeune public / Art du rue / Cantine maison et bar local / Jeux ...

## CONCERT - (19h-3h) :
* Opsa Dehëli (world latino roots - Bordeaux)
* Rubin Steiner dj set (electro - Tours)
* Sweat like an Ape (rock tropical - Bordeaux)
* Verbal Razors (hardcore - Tours)
* Solorkestar (trad roumaine - Sarthe)
* Balluche Sound System (chanson - Tours)
* Princesse Napalm (electro punk - Paris)

## SPECTACLES
### à 21h :
* Une Corde a Vécu - par la compagnie Duo Pendu (duo de corde lisse - Belgique)
### à 16h :
* Né Quelques Part - par Mathieu Barbances (Conte Musical Jeune Public sur l'exile d'une famille syrienne - Orne)

## MARCHÉ DE PRODUCTEURS LOCAUX - (15h-20h)
* Les Caprines Fromagères - Fromage de chèvre - Marçon
* Ferme Hetre rousseau - Légumes - Thoiré-sur-Dinan
* La Roche Bleue - Vins - Marçon
* Domaine De Cézin - Vins - Marçon
* Rêve d'abeilles - Miel - Le Grand-Lucé
* Ferme des Quantières - Pommes - Chenu
* Les Orties de Luna - orties transformés - Pruillé l'Éguillé
* La Ferme du Plessis - volaille - Ste Cérotte
* Le Pis Qui Chante - fromage de vaches - Villaine-sous-Lucé
* Le Cochon Zébré - ferme viticole - Marçon
* La Pâte Sarthoise - pâtes - Yvré-l'Évêque
* Le Moulin des Chevaliers - Pain et farine - Lhomme
* Jean-Claude Gasnier - Ébéniste - La Chartre-sur-le-Loir
* M'zelle Détourne - Objets en chambres à aire - La Fléche
* La Petite fabrique de papier - Papier - Chahaignes
* Zolis CouZettes - couture - St Calais
* Le Champ du Possible - savons - Droué
* Gafalasuite - illustration - Cérans-Foulletourte
* Or Ange Métallique - sculture - St Fraimbault
* La Grande Turbine du Loir - collectif d'artistes - Poncé-sur-Loir
* Pascal Riviere - maroquinerie - Vaas
* La Bonne Pioche - futur commerce à Marçon

## VILLAGE ASSOCIATIF - Après-midi (15h-20h)
* Cocami
* Greenpeace
* Ligue des Droits de l'Homme
* Réseau Éducation sans Frontière
* Union Communiste Libertaire
* Le Carrud
* Collectif Contre Amazon
* Droit au Logement
* les Arts services
* Association des Jeunes de Lavernat
* Heulâ ! Patois Sarthois

## ANIMATIONS / JEUX - Après-midi (15h-20h)
* Festy Moove - Jeux de plein air
* La Charcuterie Musicale - Blind test musical
* Frimoussette - Maquillage artistique 
* Le Palet Sarthois - le nouveau jeux ancestral bin d'chez nous !
* Bric à Notes - jeux en bois
* Espace petite enfance
* Atelier Cyanotype
* Atelier Peinture
* Atelier mosaïque
* Initiation au graffiti

## INFOS
* Cantine maison avec des produits locaux : Plat de grand-mère / Pizza au feu de bois / Bouine / Assiettes Apéro / Crêpes...
* Espace d'accueil Gratuit sur Place
* Prix Libre de 15h à 20h
* 10€ à partir de 20h / gratuit pour les moins de 12 ans
* Cet événement est organisé par les pécores sarthois de L'Asso Mnambule.
