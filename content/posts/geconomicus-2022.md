---
title: "Un Géconomicus en Sarthe le dimanche 5 juin 2022 !"
tags: ["Evenement", "Géconomicus", "Jeu"]
date: "2022-05-22"
---

# C'est quoi un Géconomicus ?
Le géconomicus est un jeu de carte qui se joue toute la journée, et qui permet de ressentir dans vos tripes ce qui se passe lorsque vous utilisez la monnaie libre par rapport à la monnaie dette.
* **Le matin** : simulation de la monnaie dette
* **Le midi** : on mange tous ensemble
* **L'après-midi** : simulation de la monnaie libre
* **En fin d'après-midi** : discussions, synthèse, et temps libre

Pour en savoir plus sur le Géconomicus : https://geconomicus.glibre.org/

# Comment s'inscrire ?
Pour vous inscrire, c'est ici :

[Fichier framacalc en ligne](https://lite.framacalc.org/jyzynr4ufg-9t5j)

Ajoutez directement votre nom/prénom/mail... dans le tableau.

# Quand ?
**Dimanche 5 juin 2022**
Ouverture du lieu au public à partir de 9h, pour **commencer à 9h30**.

# Où ?
Chez Chantal Lefevre et Patrick Crépin,
au lieu dit "Le havre de Fay"
à **Rouessé Vassé**  72140
Voir la [carte ici](https://www.google.com/maps/place/Le+Havre+de+Fay/@48.1262599,-0.2206322,15z/data=!4m5!3m4!1s0x0:0xff1ac925327f43d7!8m2!3d48.1262604!4d-0.2206319).
**Pour éviter que votre GPS vous rallonge de 10 kms et vous amène dans un champ, passez impérativement par le bourg de Rouessé Vassé!**

# Covoiturage

Voici un covoiturage possible via le site [MOVEWIZ](http://www.movewiz.fr/participation?PMW=3229ywrqMdJkQ8840)

# CR du jeu

Vous retrouverez le CR dans le [Forum de la monnaie libre](https://forum.monnaie-libre.fr/t/72-rouesse-vasse-un-geconomicus-en-sarthe-le-5-juin-2022/22777/4)
