---
title: "Jeu de la corbeille à Sainte-Suzanne le lundi 9 mai 2022"
tags: ["Evenement", "Jeu", "Archive"]
date: "2022-05-09"
---

# Communication
Le collectif 72 est invité à participer à l'organisation du Jeu de la corbeille avec un nouveau groupe du côté de Sainte-Suzanne, à la frontière entre la Sarthe et la Mayenne ! Un bon moyen de continuer à faire du lien ! :)

Plus d'infos [sur le site du Sou](https://www.le-sou.org/evenement/infog1-jeu-corbeille-stesuzanne-mai22/)

# Organisation
Yannick rejoindra Anne Amblès pour organiser une jeu de la monnaie.
