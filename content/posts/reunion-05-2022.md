---
title: "Réunion de mai (19/05/2022)"
tags: ["Evenement", "Réunion", "Géconomicus", "Outils"]
date: "2022-05-19"
---

# Où ?
**Maison des citoyen.ne.s** au Mans.

(sous la place des comtes du maine / près des cinéastes)

# Quand ?
Jeudi **19 mai à 18h30**

# ODJ de la Réunion
* Accueil des nouveaux
* Préparation du Géconomicus du 5 juin
* Le nouveau site web du collectif
* Choix d'un outil pour échanger entre les membres
* Week-end inter-régions en Suisse Normande
