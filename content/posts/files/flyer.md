# Rencontre autour de la monnaie libre

<img src="https://gitlab.com/fluidlog/sitewebml72/-/raw/main/content/posts/img/g-rencontre.png" style="width: 500px;"/>

## Présentation
**Pourquoi ?** : Pour créer un réseau d’échange de biens et de services.

**Quand ?** Le 09 OCTOBRE 2022 de 9h à 17h

**Où ?** Lieu : Chez Chantal et Patrick au « Havre de FAy », lieu-dit FAY, (72140) **ROUESSE VASSE**

## Quoi ?
La monnaie libre est un nouveau système permettant aux citoyen.ne.s de **co-créer la monnaie**.
Mais avant d'utiliser cette monnaie (qui existe déjà depuis plus de 5 ans), beaucoup d'automatismes sont à déconstruire.

Pendant ces rencontres, nous allons donc revisiter les bases de **"comment répondre à nos besoins respectifs"**, par le don, le troc ou la monnaie, et apprendre à mieux nous connaitre.

## Objectifs
1. Nous réunir entre particuliers et professionnels pour connaitre nos besoins et nos offres respectives
2. Permettre à chacun de recevoir des informations sur la monnaie libre et comprendre dans quel cas l'utiliser parmi tous les modes d'échanges possibles
3. Faire des expériences d'échange en Ĝ1 ("June") pour se familiariser avec son fonctionnement (Ĝmarché).

## Infos pratiques
Pour que ce soit concret, n’oubliez surtout pas d’amener suffisamment d’objets ou produits à échanger, chacun apporte une table d’exposition et une chaise.

**Attention:** Nous sommes au regret de **ne pouvoir accueillir les animaux** même tenus en laisse.

* **Contact :** monnaielibre72@riseup.net
* **Inscription/co-voit/programme/communication** : voir https://monnaielibre72.org/posts/rencontre-ml-2022/
