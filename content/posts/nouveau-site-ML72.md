---
title: "Un nouveau site pour le collectif ML72 !"
tags: ["Organisation", "Communication", "Outils"]
date: "2022-04-17"
---

# Communication
Après 5 ans de monnaie libre, le collectif 72 a enfin un "vrai" site internet ! :)

[https://monnaielibre72.org](https://monnaielibre72.org)

Pour les nostalgiques, ou en attendant que toutes les infos de l'ancien soient transférées sur le nouveau, voici le [lien vers l'ancien PAD](https://pad.lescommuns.org/s/SJ1OFvUoZ#).

Pour les techniciens curieux, vous trouverez [un PAD qui explique tout sur le projet du site Web](https://pad.lescommuns.org/siteWebML72)
