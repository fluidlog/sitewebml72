---
title: "Rencontre autour de la monnaie libre dimanche 9 octobre 2022 !"
tags: ["Evenement", "Gmarché", "Jeu des bouclettes"]
date: "2022-09-19"
---

![affiche](https://gitlab.com/fluidlog/sitewebml72/-/raw/main/content/posts/img/g-rencontre.png)

## Présentation
**Pourquoi ?** : Pour créer un réseau d’échange de biens et de services.

**Quand ?** Le 09 OCTOBRE 2022 de 9h à 17h

**Où ?** Lieu : Chez Chantal et Patrick au « Havre de FAy », lieu-dit FAY, (72140) **ROUESSE VASSE**

Voir la [carte ici](https://www.google.com/maps/place/Le+Havre+de+Fay/@48.1262599,-0.2206322,15z/data=!4m5!3m4!1s0x0:0xff1ac925327f43d7!8m2!3d48.1262604!4d-0.2206319).

_Pour éviter que votre GPS vous rallonge de 10 kms et vous amène dans un champ, passez impérativement par le bourg de Rouessé Vassé!_

## Quoi ?
La monnaie libre est un nouveau système permettant aux citoyen.ne.s de **co-créer la monnaie**.
Mais avant d'utiliser cette monnaie (qui existe déjà depuis plus de 5 ans), beaucoup d'automatismes sont à déconstruire.

Pendant ces rencontres, nous allons donc revisiter les bases de **"comment répondre à nos besoins respectifs"**, par le don, le troc ou la monnaie, et apprendre à mieux nous connaitre.

Selon notre compréhension du Code de Commerce, il nous semble important de préciser que **pour les Professionnels présents à cette journée, il n’y aura pas de commercialisation de produits et de services en lien avec leur activité**.

## Objectifs
1. Nous réunir entre particuliers et professionnels pour connaitre nos besoins et nos offres respectives
2. Permettre à chacun de recevoir des informations sur la monnaie libre et comprendre dans quel cas l'utiliser parmi tous les modes d'échanges possibles
3. Faire des expériences d'échange en Ĝ1 ("June") pour se familiariser avec son fonctionnement (Ĝmarché).

## Programme
- 9h : Accueil, mise en place du matériel pour les jeux et les échanges ayant lieu tout au long de la journée.

En parallèle, des cercles de discussion autour de la monnaie libre auront lieu :
- 10h : Questions/réponses : Pour les néophytes
- 11h : Questions/réponses : Pour les pros :
- 12h-14h : Repas partagé (vaisselle fournie sur site)
- 14h : Questions/réponses : Pour les néophytes
- 15h : Questions/réponses : Pour les pros :
- 17h : Débriefing et rangement

## Infos pratiques
Pour que ce soit concret, n’oubliez surtout pas d’amener suffisamment d’objets ou produits à échanger, chacun apporte une table d’exposition et une chaise.

**Attention:** Nous sommes au regret de **ne pouvoir accueillir les animaux** même tenus en laisse.

* **Contact :** monnaielibre72@riseup.net

* **Inscriptions souhaitée sur :** https://framadate.org/hYYTcP1RuBZVg6yw

(Cette inscription nous servira à estimer le nombre de partipant.e.s et à connaitre votre heure d'arrivée)

* **Covoiturage** http://www.movewiz.fr/participation?PMW=00xO5xvmFvhgw9290

(Lien permettant de nous auto-organiser)

* **Evenement Facebook** : https://www.facebook.com/events/1605695019832988
(N'hésitez pas à faire tourner ! :) )

* **Flyer de l'événement** : [Fichier à imprimer](https://gitlab.com/fluidlog/sitewebml72/-/raw/main/content/posts/files/flyer.pdf)
