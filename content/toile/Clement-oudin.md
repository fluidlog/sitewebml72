---
title: "Certification de Clément !"
tags: ["Toile"]
date: "2022-01-29"
---

# Clément Oudin
Clément est devenu membre fin janvier 2022

* profil : Oudinus
* clé publique : FsYsJ2Z8KBt5Da1CnH3vsg6E6etazCXYjug5qLFphzu8

A peine devenu membre qu'il a intégré l'équipe organisatrice du Géconomicus !
