---
title: "Qu'est-ce que la monnaie-libre ?"
tags: ["Comprendre", "blockchain"]
date: "2022-01-02"
---

# Une crypto-monnaie citoyenne et libre

- La monnaie libre est un nouveau système permettant aux citoyen.ne.s de **co-créer la monnaie** sans passer par les banques.

- Cette monnaie est **décorrélée de l’euro** contrairement aux monnaies locales complémentaires (MLC).

- Pour fonctionner de manière décentralisée et être sécurisée, cette monnaie utilise un système de type **blockchain**.

- Pour que le système de sécurité **ne consomme pas beaucoup d'énergie** (comme les autres crypto-monnaies), la monnaie libre est sécurisée par une [toile de confiance humaine](/toile).

- Pour plus d'information, voir le site [https://monnaie-libre.fr](https://monnaie-libre.fr).
