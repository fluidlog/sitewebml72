---
title: "Qu'est-ce qu'une toile de confiance ?"
tags: ["Comprendre", "toile"]
date: "2022-01-01"
---

# Un réseau humain de confiance

- Pour entrer dans le réseau de la monnaie libre, il est nécessaire d'être **certifié par 5 (bientôt 6) personnes**

- C'est à dire que la nouvelle personne doit rencontrer en vrai des membres, et échanger avec eux pour bien comprendre le fonctionnement.

- La licence de la monnaie libre liste les règles à respecter pour certifier des nouveaux membres.

- Une fois qu'on devient membre, on peut **co-créer la monnaie". Avant, on peut quand même recevoir des G1 sur son compte (dit "portefeuille"), et ainsi, en donner à d'autres.

- Cette "toile de confiance" humaine, permet de sécuriser la blockchain sans passer par des mécanismes complexes et gourmands en ressource.

Pour plus d'information sur la toile de confiance : voir le [site monnaie-libre.fr](https://monnaie-libre.fr/comprendre)
