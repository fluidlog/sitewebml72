---
title: "Comment contacter le collectif ?"
tags: ["organisation", "contact"]
---

# Par mail
Envoyez un mail à l'ensemble des membres :
monnaielibre72-membres@lists.riseup.net

# Par Facebook
* Page : https://www.facebook.com/Monnaie-libre-72-222487558227825/
* Groupe : https://www.facebook.com/groups/472070196843616/
